
function generateCoordinateProblem(width, height){
	var result = {};
	result.x = 1 + Math.floor(Math.random()*width);
	result.y  = 1 + Math.floor(Math.random()*height);
	result.asString = "("+result.x+","+result.y+")";
	return result;
}

function generatePlaceValueProblem(minValue, maxValue, size){
	var result = {};
	result.asNumbers = [];
	result.asStrings = [];
	for (var i = 0; i < size; i++){
		var success = false;
		while (!success){
			var placeValue = Math.pow(10,Math.floor(Math.random()*(maxValue - minValue)) + minValue);
			var digit = 1 + Math.floor(Math.random()*9);
			var toPut = digit/placeValue;
			if (result.asNumbers.indexOf(toPut) == -1){
				result.asNumbers.push(toPut);
				var denominator;
				if (placeValue == 10)
					denominator = ' tenth';
				else if (placeValue == 100)
					denominator = ' hundreth';
				else if (placeValue == 1000)
					denominator = ' thousandth';
					
				if (digit != 1)
					denominator = denominator + 's';
					
				result.asStrings.push(digitToString(digit)+denominator);
				success = true;
			}
		}
			
	}
	
	return result;
}

function digitToString(digit){
	switch(digit){
		case 0:
			return 'zero';
		case 1:
			return 'one';
		case 2:
			return 'two';
		case 3:
			return 'three';
		case 4:
			return 'four';
		case 5:
			return 'five';
		case 6:
			return 'six';
		case 7:
			return 'seven';
		case 8:
			return 'eight';
		case 9:
			return 'nine';
	}
}

function generateFractionProblem(){

}

function generateGeometryProblem(){

}

function generateLongDivisionProblem(){

}

function generatePaintProblem(){
	var result = {};
	var acceptableDenominators = [2,3,4,5,6,7,8,9,10,11,12];
	var d1 = n1 = d2 = n2 = 1;
	while ((n1/d1) + (n2/d2) >= 1 || LCM([d1, d2])>16){
		d1 = randomElement(acceptableDenominators);
		n1 = Math.floor(1+Math.random()*d1);
		d2 = randomElement(acceptableDenominators);
		n2 = Math.floor(1+Math.random()*d2);
	}
	result.part1 = {n:n1, d:d1};
	result.part2 = {n:n2, d:d2};
	return result;

}

function randomElement(inputArray){  return inputArray[Math.floor(Math.random()*inputArray.length)]}

//http://rosettacode.org/wiki/Least_common_multiple#JavaScript
function LCM(A){       
	var n = A.length, a = Math.abs(A[0]);
    for (var i = 1; i < n; i++){
    	var b = Math.abs(A[i]), c = a;
       	while (a && b){ 
       		a > b ? a %= b : b %= a; 
       				} 
       a = Math.abs(c*A[i])/(a+b);
     }
    return a;
}