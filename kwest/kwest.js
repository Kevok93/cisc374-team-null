var game = new Phaser.Game(800, 600, Phaser.CANVAS, "phaser-example", { preload: preload, create: create , update: update});

var asleep = {};
var entities ={};
var update_local = {};
var keyboard_keys = {};
var key_lock = {}; //stops you from pressing key every tick
var width = 90;
var people = 16;

var q_button;
var w_button;
var e_button;
var r_button;
var a_button;
var s_button;
var d_button;
var f_button;
function preload() {

	//  You can fill the preloader with as many assets as your game requires

	//  Here we are loading an image. The first parameter is the unique
	//  string by which we'll identify the image later in our code.

	//  The second parameter is the URL of the image (relative)
	//game.load.image('npc0','../assets/npc/NPC0.png');
	game.load.image('calculator','../assets/shop/Calculator.png');
	game.load.image('shop','../assets/shop/Shop.png');
	game.load.image('quarter','../assets/shop/quarter.png');
	game.load.image('dime','../assets/shop/dime.png');
	game.load.image('nickel','../assets/shop/nickel.png');
	game.load.image('penny','../assets/shop/penny.png');
	game.load.image('dd','../assets/shop/Decimal_Dude.png');
    game.load.image('splash','../assets/wordbubble.png',400,400);
	game.load.spritesheet('keys','../assets/key strip.png', width,94);
	for (var i = 0; i <= 18; i++) {
		var npc = "NPC"+i;
		var filename = "../assets/npc/"+npc+".png";
		game.load.image(npc,filename);
	}
	//game.load.spritesheet('lonk', 'assets/lonk.png',90,90);
	//game.load.image('triforce','http://fc05.deviantart.net/fs71/f/2012/052/2/5/triforce_by_chupacabrathing-d4qhvft.png');

}

function create() {

	//  This creates a simple sprite that is using our loaded image and
	//  displays it on-screen

	var shop = game.add.image(0, 0, 'shop');
	shop.width=800;
	shop.height=600;
	var calc = entities['calculator'] = game.add.sprite(600, 0, 'calculator');
	calc.width = 200;
	calc.height = 210;
	var reg = entities['calculator'] = game.add.sprite(0, 0, 'calculator');
	reg.width = 200;
	reg.height = 210;
	//entities['ground'] = game.add.image(-6,0,'ground');
	//entities['ground'].width = 816;
	//entities['ground'].height = 600;
	//entities["lonk"] = game.add.sprite(55, 32, 'lonk',1);
	update_local['quarters'] = [];
	update_local['nickels'] = [];
	update_local['pennies'] = [];
	update_local['dimes'] = [];
	update_local['register'] = Math.floor(Math.random() * 200) / 100;
	update_local['cash'] = 0;
	entities['register_string'] = game.add.text(160,36,"",{fill:"Red"});
	entities['register_string'].anchor.setTo(1, 0.5);
	entities['calculator_string'] = game.add.text(760,36,"",{fill:"Red"});
	entities['calculator_string'].anchor.setTo(1, 0.5);
	entities['timer_string'] = game.add.text(460,36,"",{fill:"Red"});
	entities['timer_string'].anchor.setTo(1, 0.5);
	entities['decimal_dude'] = game.add.sprite(630,212,'dd');
	entities['decimal_dude'].width = 200;
	entities['decimal_dude'].height = 150;
    entities['splash']=[];
	update_local['start_time'] = Date.now();

	entities['npcs'] = [];
	for(var i = 0; i < people; i++) {
		var j = Math.floor(Math.random() * 19);
		entities["npcs"][i] = game.add.sprite(650-(i*50), 300+(3*i), "NPC"+j);
		entities["npcs"][i].width = 160;
		entities["npcs"][i].height = 290;
	}
	update_local['line_length'] = people;
	game.input.keyboard.onDownCallback = function(){keyboard_keys[game.input.keyboard.event.keyCode] = true;  /*console.log(game.input.keyboard.event.keyCode);*/};
	game.input.keyboard.onUpCallback   = function(){key_lock[game.input.keyboard.event.keyCode] = false; keyboard_keys[game.input.keyboard.event.keyCode] = false; /*console.log(game.input.keyboard.event.keyCode);*/};

	game.add.text(180, 125, "+",{fontSize:'40px', fill:'red'});
	game.add.text(180, 225, "-",{fontSize:'40px', fill:'red'});
	
	q_button = game.add.sprite(210,100,"keys",26);
	q_button.inputEnabled = true;
    q_button.events.onInputUp.add(fake_key_up, this);
	q_button.events.onInputDown.add(fake_key_down, this);
	
	w_button = game.add.sprite(310,100,"keys",32);
	w_button.inputEnabled = true;
    w_button.events.onInputUp.add(fake_key_up, this);
	w_button.events.onInputDown.add(fake_key_down, this);
	
	e_button = game.add.sprite(410,100,"keys",14);
	e_button.inputEnabled = true;
    e_button.events.onInputUp.add(fake_key_up, this);
	e_button.events.onInputDown.add(fake_key_down, this);
	
	r_button = game.add.sprite(520,100,"keys",27);
	r_button.inputEnabled = true;
    r_button.events.onInputUp.add(fake_key_up, this);
	r_button.events.onInputDown.add(fake_key_down, this);
	
	a_button = game.add.sprite(200,200,"keys",10);
	a_button.inputEnabled = true;
    a_button.events.onInputUp.add(fake_key_up, this);
	a_button.events.onInputDown.add(fake_key_down, this);
	
	s_button = game.add.sprite(310,200,"keys",28);
	s_button.inputEnabled = true;
    s_button.events.onInputUp.add(fake_key_up, this);
	s_button.events.onInputDown.add(fake_key_down, this);
	
	d_button = game.add.sprite(410,200,"keys",13);
	d_button.inputEnabled = true;
    d_button.events.onInputUp.add(fake_key_up, this);
	d_button.events.onInputDown.add(fake_key_down, this);
	
	f_button = game.add.sprite(515,200,"keys",15);
	f_button.inputEnabled = true;
    f_button.events.onInputUp.add(fake_key_up, this);
	f_button.events.onInputDown.add(fake_key_down, this);
	
	var temp;
	temp = game.add.image(225,50,'quarter');
	temp.width  = 50;
	temp.height = 50;
	temp = game.add.image(325,50,'dime');
	temp.width  = 50;
	temp.height = 50;
	temp = game.add.image(431,50,'nickel');
	temp.width  = 50;
	temp.height = 50;
	temp = game.add.image(535,50,'penny');
	temp.width  = 50;
	temp.height = 50;
}

function update() {keypad();draw();}

function fake_key_down(sprite, pointer){
	var code = 0;
	switch(sprite){
		case q_button:
			code = 81;
			break;
		case a_button:
			code = 65;
			break;
		case w_button:
			code = 87;
			break;
		case s_button:
			code = 83;
			break;
		case e_button:
			code = 69;
			break;
		case d_button:
			code = 68;
			break;	
		case r_button:
			code = 82;
			break;
		case f_button:
			code = 70;
			break;	
	}
	keyboard_keys[code] = true;
}

function fake_key_up(sprite, pointer){
	var code = 0;
	switch(sprite){
		case q_button:
			code = 81;
			break;
		case a_button:
			code = 65;
			break;
		case w_button:
			code = 87;
			break;
		case s_button:
			code = 83;
			break;
		case e_button:
			code = 69;
			break;
		case d_button:
			code = 68;
			break;	
		case r_button:
			code = 82;
			break;
		case f_button:
			code = 70;
			break;	
	}
	key_lock[code] = false;
	//keyboard_keys[game.input.keyboard.event.keyCode] = false; 
}

function keypad(sprite, pointer) {
	if (entities['npcs'].length==0) {
        if (getBool(keyboard_keys,32 )) { //space or enter
			var progress;
        	if (sessionStorage.getItem('progress')){
        		progress = JSON.parse(sessionStorage.getItem('progress'));
        	} else {
				progress = {"paintShop":false,"quarry":false,"table":false,"shop":false,"farm":false,"shoe":false};
			}
       		progress.shop = true;
       		sessionStorage.setItem('progress', JSON.stringify(progress));
            window.location.href = "../overworld.html";
        }
        return;
    }
	if (getBool(keyboard_keys,81 ) && !getBool(key_lock,81 )) {update_local['quarters'].push(addCoin(191,100,'quarter'));key_lock[81 ]=true;}
	if (getBool(keyboard_keys,65 ) && !getBool(key_lock,65 )) {pop(update_local['quarters']);key_lock[65 ]=true;}
	if (getBool(keyboard_keys,87 ) && !getBool(key_lock,87 )) {update_local['dimes'].push(addCoin(299,100,'dime'));key_lock[87 ]=true;}
	if (getBool(keyboard_keys,83 ) && !getBool(key_lock,83 )) {pop(update_local['dimes']);key_lock[83 ]=true;}
	if (getBool(keyboard_keys,69 ) && !getBool(key_lock,69 )) {update_local['nickels'].push(addCoin(407,100,'nickel'));key_lock[69 ]=true;}
	if (getBool(keyboard_keys,68 ) && !getBool(key_lock,68 )) {pop(update_local['nickels']);key_lock[68 ]=true;}
	if (getBool(keyboard_keys,82 ) && !getBool(key_lock,82 )) {update_local['pennies'].push(addCoin(515,100,'penny'));key_lock[82 ]=true;}
	if (getBool(keyboard_keys,70 ) && !getBool(key_lock,70 )) {pop(update_local['pennies']);key_lock[70 ]=true;}
	if (update_local['cash'] == update_local['register'] && getBool(keyboard_keys,13 ) && !getBool(key_lock,13 )) {
		key_lock[13 ]=true;
		while (pop(update_local['pennies'])) ;
		while (pop(update_local['nickels'])) ;
		while (pop(update_local['quarters'])) ;
		while (pop(update_local['dimes'])) ;
		update_local['cash'] = 0.0;
		update_local['register'] = Math.floor(Math.random() * 199 + 1) / 100;
		entities['npcs'].shift().destroy();
		entities['npcs'].shift().destroy();
		for (var i = 0; i < entities['npcs'].length; i++) {
			entities['npcs'][i].x +=100;
			entities['npcs'][i].y -=6;
		}
        if (entities['npcs'].length == 0) {
            var dt = Math.floor((Date.now() - update_local['start_time'])/1000);
            var seconds = dt % 60;
            seconds = (seconds < 10) ? ("0"+seconds) : seconds;
            var minutes = Math.floor(dt / 60);

            var splash = entities["splash"];
            var bub = game.add.image(100, 50, 'splash');
            bub.height = 350;
            bub.width = 550;
            splash[splash.length] = bub;
            splash[splash.length] = game.add.text(330, 300, "[press space]", {fontSize:'12px', fill:"black"});
            splash[splash.length] = game.add.text(280, 70, "  *Thanks for your help!*", {fontSize:'25px', fill:"black"});
            splash[splash.length] = game.add.text(175, 175, "Your final time was:  "+minutes + ":" + seconds, {fontSize:'25px', fill:"black"});
        }
	}
}

function addCoin(x,y,type) {
	var absx = x + (Math.random() * 30) - 20;
	var absy = y + (Math.random() * 30) - 20;
	return game.add.sprite(absx, absy, type);
}
function use() {

}
function draw() {
	update_local['cash'] = Math.floor(
		(update_local['quarters'].length * 25) +
		(update_local['nickels'].length * 05)+
		(update_local['pennies'].length * 01)+ 
		(update_local['dimes'].length * 10))/100;
	if (update_local['line_length']!=0) {
		var dt = Math.floor((Date.now() - update_local['start_time'])/1000);
		var seconds = dt % 60;
		seconds = (seconds < 10) ? ("0"+seconds) : seconds;
		var minutes = Math.floor(dt / 60);

		entities['timer_string'].text = minutes + ":" + seconds;
	}
	entities['calculator_string'].text = update_local['cash'].toFixed(2);
	entities['register_string'].text = update_local['register'].toFixed(2);
}
function pop(array) {
	if (array.length > 0) {
		array.pop().destroy();
		return true;
	} else return false;
}

function getBool(hash,key) {return ((key in hash) && (hash[key]));}
function sleep(key,milliseconds) {
	var timer = game.time.events;
	asleep[key] = true; 
	timer.add(milliseconds,function(){asleep[key]=false;});
}

function end_handler(){

}
