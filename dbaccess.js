var get_table = function (table_name) {
	var command = "db."+table_name+".find().toArray();";
	return raw_command(command);
};

var add_row = function (table_name, row) {
	var command = "db."+table_name+".insert("+JSON.stringify(row)+");";
	return raw_command(command);
};

var update_row = function (table_name, id, row) {
	var command = 
		"db."+table_name+".update("+
			"{_id:"+id+"},"+
			"{$set:"+row+"},"+
			"{}"+
		");";
	return raw_command(command);
};

var raw_command = function (command) {
	var output;
    $.ajax({
        async: false,
        url: './db/',
        type: "GET",
        data: {"sql": command},
        success: function (data) {
			output = JSON.parse(data);
        },
        error: function () {
			output = {"error":"error"};
        }
    });
	return output;
}
