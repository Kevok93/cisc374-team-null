  function drawCircle(centerX, centerY, radius, lineColor, fillColor, graphics){

          graphics.clear();
          graphics.lineStyle(3, lineColor, .5);
          graphics.beginFill(fillColor, 1);
          graphics.drawEllipse(centerX, centerY, radius, radius);

        }

        function drawArc(centerX, centerY, radius, angles, colors, graphics){

          if (angles.length > 1){
            var startAngle;
            var endAngle;
            drawCircle(centerX, centerY, radius, 0xffffff, colors[0]);

            startAngle = -PI/2 ;
            graphics.lineStyle(2,0x000000, 0);

            for (var i = 0; i < angles.length - 1; i++){
              //startAngle = angles[i];
              endAngle = startAngle - angles[i];
              graphics.beginFill(colors[i]);
              console.log("drawing (" +startAngle +", "+endAngle+")");
              graphics.arc(centerX, centerY, radius, startAngle , endAngle , true );
              startAngle = endAngle;
            }
            if (endAngle != -PI/2){
              graphics.beginFill(0x000000);

              graphics.arc(centerX, centerY, radius, endAngle , -PI/2 , true );

            }

          }
          graphics.endFill()

        }


        function fractionsToAngles(fractions){
          var result = [0,0,0];
          for (var i = 0; i < fractions.length; i++){
            result[i] = (fractionToAngle(fractions[i]));

          }
          return result;

        }
        function fractionToAngle(portion){
          return (2*PI)*portion;

        }
        function divideCircle(centerX, centerY, radius, lineColor, sections, graphics){
          graphics.beginFill(0x000000,0);
          var increment = 2*PI/sections;
          var startAngle = -PI/2;
          var endAngle;
          for (var i = 0; i < sections; i++){
            //startAngle = angles[i];
            endAngle = startAngle - increment;
            graphics.lineStyle(2, lineColor, .8);
            console.log("drawing (" +startAngle +", "+endAngle+")");
            graphics.arc(centerX, centerY, radius, startAngle , endAngle , true );
            startAngle = endAngle;
          }
          graphics.endFill();

        }